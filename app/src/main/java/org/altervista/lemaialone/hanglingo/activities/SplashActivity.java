package org.altervista.lemaialone.hanglingo.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.activities.categories.MainActivity;
import org.altervista.lemaialone.hanglingo.data.Category;
import org.altervista.lemaialone.hanglingo.data.DataProvider;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {
	
	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
	}

	@Override protected void onStart(){
		super.onStart();

		new AsyncTask<Void, Void, ArrayList<Category>>() {
			@Override protected ArrayList<Category> doInBackground(Void... params){
				DataProvider provider = DataProvider.getInstance(SplashActivity.this);
				return new ArrayList<>(provider.getCategories());
			}

			@Override protected void onPostExecute(ArrayList<Category> list){
				Intent intent = new Intent(SplashActivity.this, MainActivity.class);
				intent.putParcelableArrayListExtra(MainActivity.LIST_EXTRA, list);
				startActivity(intent);
				finish();
			}
		}.execute();
	}
}
