package org.altervista.lemaialone.hanglingo.widgets.keyboard;

import android.inputmethodservice.KeyboardView;

/**
 * Empty OnKeyboardActiobListener to avoid declaring 8 methods when you need only one of them.
 *
 * Created by Ilario Sanseverino on 08/11/15.
 */
public class KeyboardAdapter implements KeyboardView.OnKeyboardActionListener {
	@Override public void onPress(int primaryCode){

	}

	@Override public void onRelease(int primaryCode){

	}

	@Override public void onKey(int primaryCode, int[] keyCodes){

	}

	@Override public void onText(CharSequence text){

	}

	@Override public void swipeLeft(){

	}

	@Override public void swipeRight(){

	}

	@Override public void swipeDown(){

	}

	@Override public void swipeUp(){

	}
}
