package org.altervista.lemaialone.hanglingo.widgets.progress;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

import static org.altervista.lemaialone.hanglingo.widgets.progress.OverlayDrawer.*;

/**
 * This class shows a partially revealed image. The way in which the partial revealing is to be
 * performed is determined by the {@link OverlayDrawer} attached.
 *
 * Created by Ilario Sanseverino on 11/11/15.
 */
public class ProgressRevealDrawable extends ImageView {
	private OverlayDrawer mOverlayDrawer;
	protected int mProgress;

	public ProgressRevealDrawable(Context context){
		this(context, null);
	}

	public ProgressRevealDrawable(Context context, AttributeSet attrs){
		this(context, attrs, 0);
	}

	public ProgressRevealDrawable(Context context, AttributeSet attrs, int defStyleAttr){
		super(context, attrs, defStyleAttr);
	}

	@Override public void draw(Canvas canvas){
		super.draw(canvas);
		if(getDrawable() != null && mOverlayDrawer != null)
			mOverlayDrawer.draw(canvas, mProgress, getWidth(), getHeight());
	}

	/**
	 * Set the percentage of image tobe revealed.
	 *
	 * @param progress an integer between 0 and {@link OverlayDrawer#MAX_PROGRESS}. Values outside
	 * this range will be set to 0 if negative, MAX_PROGRESS otherwise.
	 */
	public void setProgress(int progress){
		mProgress = Math.max(0, Math.min(MAX_PROGRESS, progress));
		invalidate();
	}

	/**
	 * @return the currently set progress.
	 */
	@SuppressWarnings("unused") public int getProgress(){
		return mProgress;
	}

	/**
	 * Set the OverlayDrawer used to partially hide the underlying image.
	 *
	 * @param overlayDrawer the {@link OverlayDrawer} to set.
	 */
	public void setOverlayDrawer(OverlayDrawer overlayDrawer){
		mOverlayDrawer = overlayDrawer;
	}
}
