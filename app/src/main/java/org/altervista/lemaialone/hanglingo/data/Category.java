package org.altervista.lemaialone.hanglingo.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Data object for the category list retrieved from the server.
 *
 * Created by Ilario Sanseverino on 12/10/15.
 */
public class Category implements Parcelable {
	public static final Creator<Category> CREATOR = new Creator<Category>() {
		@Override public Category createFromParcel(Parcel in){
			return new Category(in);
		}

		@Override public Category[] newArray(int size){
			return new Category[size];
		}
	};
	public final long id;
	public final String name;
	public final String imageURI;
	public final int records;

	public Category(long id, String name, String imageURI, int records){
		this.id = id;
		this.name = name;
		this.imageURI = imageURI;
		this.records = records;
	}

	protected Category(Parcel in){
		id = in.readLong();
		name = in.readString();
		imageURI = in.readString();
		records = in.readInt();
	}

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(imageURI);
		dest.writeInt(records);
	}
}
