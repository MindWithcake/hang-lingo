package org.altervista.lemaialone.hanglingo.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.data.Kandinskij;

import java.util.Locale;

import static android.util.TypedValue.*;

/**
 * This class is used to display a word or sentence inside the hangman picture.
 *
 * Created by Ilario Sanseverino on 25/10/15.
 */
public class WordView extends ImageView {
	private final static String DROID_SANSE = "fonts/DroidSanseMono.ttf";

	private final RectF mWordRect;
	private Paint mWordPaint;
	private String mWord;
	private float mTextBottom;

	public WordView(Context context){
		this(context, null);
	}

	public WordView(Context context, AttributeSet attrs){
		this(context, attrs, 0);
	}

	public WordView(Context context, AttributeSet attrs, int defStyleAttr){
		super(context, attrs, defStyleAttr);

		setImageResource(R.drawable.hangman0);

		DisplayMetrics metrics = getResources().getDisplayMetrics();
		mWordRect = new RectF(
				applyDimension(COMPLEX_UNIT_DIP, 15, metrics),
				applyDimension(COMPLEX_UNIT_DIP, 240, metrics),
				applyDimension(COMPLEX_UNIT_DIP, 260, metrics),
				applyDimension(COMPLEX_UNIT_DIP, 294, metrics));

		mWordPaint = new TextPaint();
		mWordPaint.setTextAlign(Paint.Align.CENTER);
		mWordPaint.setAntiAlias(true);
		mWordPaint.setTypeface(Kandinskij.getFont(context, DROID_SANSE));
		mWordPaint.setColor(Color.WHITE);

		if(isInEditMode())
			setWord("H_ng__n g__e");
	}

	public void setWord(String word){
		setWord(word, Locale.US);
	}

	public void setWord(String word, Locale locale){
		mWord = word.toUpperCase(locale);

		float textSize = 8f, textHeight;
		Rect bounds = new Rect();
		do{
			mWordPaint.setTextSize(++textSize);
			Paint.FontMetrics metrics = mWordPaint.getFontMetrics();
			mWordPaint.getTextBounds(word, 0, word.length(), bounds);
			mTextBottom = metrics.bottom;
			textHeight = metrics.bottom-metrics.top;
		} while(mWordRect.height() > textHeight && mWordRect.width() > bounds.width());

		mWordPaint.setTextSize(textSize);
		invalidate();
	}

	public void setImageStep(int step){
		TypedArray array = getResources().obtainTypedArray(R.array.hangmen);
		try{
			setImageDrawable(array.getDrawable(step));
		}
		finally{
			array.recycle();
		}
	}

	@Override protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		if(mWord != null){
			canvas.concat(getImageMatrix());
			canvas.drawText(mWord, mWordRect.centerX(), mWordRect.bottom - mTextBottom, mWordPaint);

		}
	}
}
