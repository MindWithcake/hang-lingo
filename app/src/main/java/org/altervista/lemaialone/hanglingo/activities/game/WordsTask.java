package org.altervista.lemaialone.hanglingo.activities.game;

import android.os.AsyncTask;

import org.altervista.lemaialone.hanglingo.data.Word;

import java.util.List;

/**
 * Async task to get the list of available words for the given category.
 *
 * Created by Ilario Sanseverino on 17/11/15.
 */
class WordsTask extends AsyncTask<Void, String, List<Word>> {
	private GameFlowActivity mGameFlowActivity;
	private final long mCategoryID;
	private boolean running = true;

	public WordsTask(GameFlowActivity gameFlowActivity, long category){
		mGameFlowActivity = gameFlowActivity;
		mCategoryID = category;
	}

	@Override protected List<Word> doInBackground(Void... params){
		new Thread(new Runnable() {
			@Override public void run(){
				while(running){
					char[] loading = "LOADING".toCharArray();
					for(int i = 0; i < loading.length; ++i){
						if(GameFlowActivity.RAND.nextBoolean())
							loading[i] = '_';
					}
					publishProgress(new String(loading));
					try{ Thread.sleep(500); }
					catch(InterruptedException ignored){}
				}
			}
		}).start();

		List<Word> list = mGameFlowActivity.provider.getWords(mCategoryID);
		running = false;
		return list;
	}

	@Override protected void onProgressUpdate(String... values){
		mGameFlowActivity.showLoadingProgress(values[0]);
	}

	@Override protected void onPostExecute(List<Word> list){
		mGameFlowActivity.mWords = list;
		mGameFlowActivity.initMatch();
	}
}
