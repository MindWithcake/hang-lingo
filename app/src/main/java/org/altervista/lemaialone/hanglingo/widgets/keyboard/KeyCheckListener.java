package org.altervista.lemaialone.hanglingo.widgets.keyboard;

/**
 * This interface is used by the {@link HangKeyboardView} to check the correctness of the
 * selected character.
 *
 * Created by Ilario Sanseverino on 09/11/15.
 */
public interface KeyCheckListener {
	/**
	 * Check whether the chosen key is wrong or right
	 * @param keycode the key chosen by the user
	 * @return true if the letter is part of the word, false otherwise
	 */
	boolean onKey(int keycode);
}
