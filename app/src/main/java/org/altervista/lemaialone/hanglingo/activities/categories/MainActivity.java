package org.altervista.lemaialone.hanglingo.activities.categories;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.data.Category;

import java.util.List;

public class MainActivity extends AppCompatActivity {
	public static final String LIST_EXTRA = "org.altervista.lemaialone.hanglingo.Main.LIST";

	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		RecyclerView recyclerView = (RecyclerView)findViewById(R.id.category_recycler);
		int columns = getResources().getInteger(R.integer.category_columns);
		LayoutManager layoutManager = new GridLayoutManager(this, columns);

		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(layoutManager);

		List<Category> categories = getIntent().getParcelableArrayListExtra(LIST_EXTRA);
		RecyclerView.Adapter adapter = new CategoryAdapter(categories);
		recyclerView.setAdapter(adapter);
	}
}
