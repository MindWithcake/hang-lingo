package org.altervista.lemaialone.hanglingo.activities.game;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.altervista.lemaialone.hanglingo.data.Category;
import org.altervista.lemaialone.hanglingo.data.DataProvider;
import org.altervista.lemaialone.hanglingo.data.Word;
import org.altervista.lemaialone.hanglingo.widgets.keyboard.KeyCheckListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class implements the logic part of the game. Concrete subclasses must take care of the
 * visual feedback.
 *
 * Created by Ilario Sanseverino on 17/11/15.
 */
public abstract class GameFlowActivity extends AppCompatActivity implements KeyCheckListener {
	public final static String CATEGORY_EXTRA = "GameActivity.CATEGORY";
	final static Random RAND = new Random();
	private final static String WORDS_TAG = "GameActivity.WORDS_TAG";
	private final static String GAME_TAG = "GameActivity.GAME_TAG";
	private final static String IN_GAME_TAG = "GameActivity.IN_GAME_TAG";

	private HangmanGame mGame;
	private int mCurrentStreak, mBestStreak;
	private boolean inGame = false;
	private WordsTask mTask;

	protected Category mCategory;
	protected final DataProvider provider = DataProvider.getInstance(this);
	protected List<Word> mWords;

	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mCategory = getIntent().getParcelableExtra(CATEGORY_EXTRA);
		setTitle(mCategory.name);

		if(savedInstanceState != null){
			mWords = savedInstanceState.getParcelableArrayList(WORDS_TAG);
			mGame = savedInstanceState.getParcelable(GAME_TAG);
			inGame = savedInstanceState.getBoolean(IN_GAME_TAG, false);
		}
	}

	@Override protected void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList(WORDS_TAG, new ArrayList<>(mWords));
		outState.putParcelable(GameFlowActivity.GAME_TAG, mGame);
		outState.putBoolean(IN_GAME_TAG, inGame);
	}

	@Override public boolean onKey(int keycode){
		inGame = true;
		boolean result = mGame.guess((char)keycode);

		if(result)
			advance(mGame.getShownString(), mGame.getProgress());
		else
			fail(mGame.getTries());

		if(mGame.hasWon())
			winGame();
		else if(mGame.hasLost())
			loseGame();

		return result;
	}

	@Override protected void onStart(){
		super.onStart();
		mCurrentStreak = provider.getCurrentStreak();
		mBestStreak = provider.getBestStreak();
		setScore(mCurrentStreak, mBestStreak);

		if(mWords == null)
			(mTask = new WordsTask(this, mCategory.id)).execute();
		else if(mGame == null)
			initMatch();
		else
			resumeGame(mGame.getShownString(), mGame.getTries());
	}

	@Override protected void onStop(){
		super.onStop();
		if(mTask != null)
			mTask.cancel(true);
		provider.saveCurrentStreak(mCurrentStreak);
		provider.saveBestStreak(mBestStreak);
	}

	@Override public void onBackPressed(){
		super.onBackPressed();
		if(inGame)
			mCurrentStreak = 0;
	}

	void initMatch(){
		if(mWords.size() > 0){
			int nextWord = RAND.nextInt(mWords.size());
			Word next = mWords.get(nextWord);
			mGame = new HangmanGame(next);
			startMatch(mGame.getShownString(), next.imageURI);
		}
		else
			noMatch();
	}

	private void winGame(){
		++mCurrentStreak;
		if(mCurrentStreak > mBestStreak)
			mBestStreak = mCurrentStreak;
		endGame(true);
	}

	private void loseGame(){
		mCurrentStreak = 0;
		endGame(false);
	}

	private void endGame(boolean isWon){
		setScore(mCurrentStreak, mBestStreak);
		inGame = false;
		endGame(isWon, mGame.getWord());
		mGame = null;
	}

	protected abstract void startMatch(String word, String imageUri);

	protected abstract void noMatch();

	protected abstract void setScore(int current, int best);

	protected abstract void advance(String word, float progress);

	protected abstract void fail(int tries);

	protected abstract void endGame(boolean isWon, String word);

	protected abstract void showLoadingProgress(String progress);

	protected abstract void resumeGame(String word, int tries);
}
