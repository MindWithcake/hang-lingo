package org.altervista.lemaialone.hanglingo.activities.categories;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.data.Category;

import java.util.List;

/**
 * Adapter for the category grid.
 *
 * Created by Ilario Sanseverino on 13/10/15.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
	List<Category> mDataSet;

	public CategoryAdapter(List<Category> dataSet){
		mDataSet = dataSet;
	}

	@Override public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.view_category, parent, false);
		return new CategoryHolder(view);
	}

	@Override public void onBindViewHolder(CategoryHolder holder, int position){
		holder.populate(mDataSet.get(position));
	}

	@Override public int getItemCount(){
		return mDataSet.size();
	}
}
