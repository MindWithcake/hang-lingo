package org.altervista.lemaialone.hanglingo.activities.game;

import android.os.Parcel;
import android.os.Parcelable;

import org.altervista.lemaialone.hanglingo.data.Word;

/**
 * This class manages the game logic (showing/hiding letters, decide whether the game is won or
 * lost, ...).
 *
 * Created by Ilario Sanseverino on 09/11/15.
 */
public class HangmanGame implements Parcelable {
	public final static int MAX_ERRORS = 4;

	private Word mWord;
	private char[] mShown;
	private int mTries = 0;

	public HangmanGame(Word word){
		mWord = word;
		mShown = mWord.word.replaceAll("[^\\s]", "_").toCharArray();
	}

	protected HangmanGame(Parcel in){
		mWord = in.readParcelable(Word.class.getClassLoader());
		mShown = in.createCharArray();
		mTries = in.readInt();
	}

	@Override public void writeToParcel(Parcel dest, int flags){
		dest.writeParcelable(mWord, flags);
		dest.writeCharArray(mShown);
		dest.writeInt(mTries);
	}

	@Override public int describeContents(){
		return 0;
	}

	public static final Creator<HangmanGame> CREATOR = new Creator<HangmanGame>() {
		@Override public HangmanGame createFromParcel(Parcel in){
			return new HangmanGame(in);
		}

		@Override public HangmanGame[] newArray(int size){
			return new HangmanGame[size];
		}
	};

	public boolean guess(char letter){
		boolean correct = false;
		String word = mWord.word.toUpperCase();
		for(int i = 0; i < word.length(); ++i){
			if(word.charAt(i) == letter){
				correct = true;
				mShown[i] = letter;
			}
		}

		if(!correct)
			++mTries;
		return correct;
	}

	public String getShownString(){ return new String(mShown); }

	public String getWord(){ return mWord.word.toUpperCase(); }

	public String getImage() { return mWord.imageURI; }

	public boolean hasWon(){
		for(char c: mShown){
			if(c == '_')
				return false;
		}
		return true;
	}

	public boolean hasLost(){ return mTries >= MAX_ERRORS; }

	public int getTries(){ return mTries; }

	public float getProgress(){
		float guessed = 0;
		for(char c: mShown){
			if(c != '_')
				++guessed;
		}
		return guessed / mShown.length;
	}
}
