package org.altervista.lemaialone.hanglingo.widgets.progress;

import android.graphics.Canvas;
import android.util.Log;

import java.util.Random;

/**
 * Interface for partially covering an image. Given a canvas to paint on and a progress value
 * this object should reveal only a percentage of the canvas. The value of the percentage is given
 * by the progress parameter.
 *
 * Created by Ilario Sanseverino on 11/11/15.
 */
public interface OverlayDrawer {
	/**
	 * the progress value for a 100% revealed image.
	 */
	int MAX_PROGRESS = 100;

	/**
	 * Partially cover the given canvas.
	 *
	 * @param canvas the canvas to cover.
	 * @param progress fraction of the canvas to display, between 0 and {@link #MAX_PROGRESS}.
	 * @param width the width of the canvas.
	 * @param height the height of the canvas.
	 */
	 void draw(Canvas canvas, int progress, float width, float height);

	enum Drawers{
		RandomCheckered(RandomCheckeredDrawer.class),
		Radial(RadialDrawer.class),
		CornerRadial(CornerRadialDrawer.class);

		public final Class<? extends OverlayDrawer> mDrawer;
		private final static Random RAND = new Random();
		private final static Drawers[] drawers = values();

		Drawers(Class<? extends OverlayDrawer> drawer){
			mDrawer = drawer;
		}

		public static OverlayDrawer getNextDrawer(){
			try{
				return drawers[RAND.nextInt(drawers.length)].mDrawer.newInstance();
			}
			catch(Exception e){
				Log.e("Hang", "Exception creating drawer", e);
				return null;
			}
		}
	}
}
