package org.altervista.lemaialone.hanglingo.activities.categories;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.activities.game.GameActivity;
import org.altervista.lemaialone.hanglingo.activities.game.GameFlowActivity;
import org.altervista.lemaialone.hanglingo.data.Category;
import org.altervista.lemaialone.hanglingo.data.Kandinskij;

/**
 * ViewHolder for the category adapter.
 *
 * Created by Ilario Sanseverino on 13/10/15.
 */
public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private final Context mContext;
	private TextView mTextView;
	private ImageView mImageView;
	private CardView mCard;
	@SuppressWarnings("FieldCanBeLocal") private Target mTarget;
	private Category mCategory;

	public CategoryHolder(View itemView){
		super(itemView);
		mTextView = (TextView)itemView.findViewById(R.id.category_label);
		mImageView = (ImageView)itemView.findViewById(R.id.category_image);
		mCard = (CardView)itemView.findViewById(R.id.category_card);
		mContext = itemView.getContext();
		itemView.setOnClickListener(this);
	}

	public void populate(Category category){
		mTextView.setText(category.name);
		mCategory = category;
		if(!category.imageURI.isEmpty()){
			mTarget = new Target() {
				@Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from){
					mImageView.setImageBitmap(bitmap);
					new Palette.Builder(bitmap).generate(new Palette.PaletteAsyncListener() {
						@Override public void onGenerated(Palette palette){
							Palette.Swatch swatch = palette.getVibrantSwatch();
							if(swatch == null)
								swatch = palette.getDarkVibrantSwatch();
							if(swatch == null)
								swatch = palette.getLightMutedSwatch();
							if(swatch == null)
								swatch = palette.getMutedSwatch();

							if (swatch != null) {
								mTextView.setBackgroundColor(swatch.getRgb());
								mTextView.setTextColor(swatch.getTitleTextColor());
								mTextView.getBackground().setAlpha(192);
								mCard.setCardBackgroundColor(swatch.getRgb());
							}
						}
					});
				}

				@Override public void onBitmapFailed(Drawable errorDrawable){
					mImageView.setImageDrawable(errorDrawable);
				}

				@Override public void onPrepareLoad(Drawable placeHolderDrawable){
					mImageView.setImageDrawable(placeHolderDrawable);
				}
			};

			Kandinskij.load(mContext, category.imageURI).into(mTarget);
			/*new Picasso.Builder(mContext).downloader(new OkHttpDownloader(mContext))
					.build()
					.load(category.imageURI)
					.into(mTarget);*/
		}
	}

	@Override public void onClick(View v){
		Intent intent = new Intent(mContext, GameActivity.class);
		intent.putExtra(GameFlowActivity.CATEGORY_EXTRA, mCategory);
		mContext.startActivity(intent);
	}
}
