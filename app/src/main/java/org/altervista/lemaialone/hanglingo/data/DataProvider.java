package org.altervista.lemaialone.hanglingo.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.util.Log;

import org.altervista.lemaialone.hanglingo.BuildConfig;
import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.widgets.progress.OverlayDrawer;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.altervista.lemaialone.hanglingo.data.ServerConstants.*;

/**
 * This class provides abstract data fetching from the actual backend used. Original data can be a
 * resource file, contained in a local database or got through the web, activities don't have to
 * care about it. Since there is no guarantee that data is immediately available, users must avoid
 * calling method of this class on the main thread.
 * <p/>
 * Created by Ilario Sanseverino on 13/10/15.
 */
public class DataProvider {
	private final static String BEST_STREAK_KEY = "BEST_STREAK";
	private final static String CURRENT_STREAK_KEY = "CURRENT_STREAK";
	private final static String PREFERENCES_NAME = "HangLingoPref";

	private static DataProvider instance;

	private final Context mContext;

	/**
	 * Private constructor for the enforcement of the Singleton design pattern.
	 * @param context the caller Context, used to access resources.
	 */
	private DataProvider(Context context){mContext = context;}

	/**
	 * @param context the caller Context.
	 * @return the singleton instance of this class.
	 */
	public static DataProvider getInstance(Context context){
		if(instance == null)
			instance = new DataProvider(context);

		return instance;
	}

	/**
	 * @return the list of all the available categories.
	 */
	public List<Category> getCategories(){
		SoapObject result = performRequest(categoryRequest(), ACTION_CATEGORIES);
		ArrayList<Category> toReturn = new ArrayList<>();

		if(result != null){
			SoapObject list = (SoapObject)result.getProperty(0);
			for(int i = 0; i < list.getPropertyCount(); ++i){
				SoapObject category = (SoapObject)list.getProperty(i);
				String name = category.getProperty(CATEGORY_NAME).toString();
				long id = Long.parseLong(category.getProperty(CATEGORY_ID).toString());
				int count = Integer.parseInt(category.getProperty(CATEGORY_COUNT).toString());
				String image = category.getPrimitiveProperty(CATEGORY_IMAGE).toString();
				toReturn.add(new Category(id, name, image, count));
			}
		}

		return toReturn;
	}

	/**
	 * Get all the known words for the given category.
	 *
	 * @param category ID of the required category
	 * @return a list of Words
	 */
	public List<Word> getWords(long category){
		SoapObject result = performRequest(wordRequest(category), ACTION_WORDS);
		ArrayList<Word> toReturn = new ArrayList<>();

		if(result != null){
			SoapObject list = (SoapObject)result.getProperty(0);
			for(int i = 0; i < list.getPropertyCount(); ++i){
				SoapObject word = (SoapObject)list.getProperty(i);
				String content = word.getPropertyAsString(WORD_CONTENT);
				String image = word.getPropertyAsString(WORD_IMAGE);
				toReturn.add(new Word(content, image));
			}
		}

		return toReturn;
	}

	/**
	 * Get the keyboard layout based on the current selected language. At the moment only
	 * Te Reo Māori is supported.
	 *
	 * @return a {@link Keyboard} instance with a layout fit for the game language.
	 */
	public Keyboard getKeyboard(){
		return new Keyboard(mContext, R.xml.maori_keyboard);
	}

	public void saveCurrentStreak(int streak){
		getPref().edit().putInt(CURRENT_STREAK_KEY, streak).apply();
	}

	public void saveBestStreak(int streak){
		getPref().edit().putInt(BEST_STREAK_KEY, streak).apply();
	}

	public int getCurrentStreak(){
		return getPref().getInt(CURRENT_STREAK_KEY, 0);
	}

	public int getBestStreak(){
		return getPref().getInt(BEST_STREAK_KEY, 0);
	}

	public OverlayDrawer nextOverlay(){
		return OverlayDrawer.Drawers.getNextDrawer();
	}

	private SharedPreferences getPref(){
		return mContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
	}

	/**
	 * Internal method to perform an actual request to a .NET web server.
	 *
	 * @param request a SOAPObject with the output parameters.
	 * @param action name of the service to call.
	 * @return a SOAPObject with the server response, or {@code null} if an error occurred.
	 */
	private SoapObject performRequest(SoapObject request, String action){
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		try{
			HttpTransportSE transport = new HttpTransportSE(SERVER_URL);
			transport.call(action, envelope);
			if(envelope.bodyIn instanceof SoapObject)
				return (SoapObject)envelope.bodyIn;
			return null;
		}
		catch(XmlPullParserException | IOException e){
			if(BuildConfig.DEBUG)
				Log.d("Net", "Request error", e);
			return null;
		}
	}
}
