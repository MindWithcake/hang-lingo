package org.altervista.lemaialone.hanglingo.widgets.progress;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;

/**
 * {@link OverlayDrawer} implementation covering the image Created by Ilario Sanseverino on
 * 12/11/15.
 */
public class RadialDrawer implements OverlayDrawer {
	private final Paint mPaint;

	public RadialDrawer(){
		mPaint = new Paint();
		mPaint.setColor(Color.BLACK);
		mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		mPaint.setAntiAlias(true);
	}

	@Override public void draw(Canvas canvas, int progress, float width, float height){
		if(progress == MAX_PROGRESS)
			return;
		if(progress == 0){
			canvas.drawPaint(mPaint);
			return;
		}

		float centerX = width / 2, centerY = height / 2;
		float shownRatio = (float)progress / MAX_PROGRESS;
		float vOff = centerY * shownRatio;
		float hOff = centerX + centerX * shownRatio;

		Path path = new Path();
		path.moveTo(centerX, centerY);
		path.lineTo(width, centerY);
		path.lineTo(width, vOff);
		path.lineTo(centerX, centerY);
		path.lineTo(width, 0f);
		path.lineTo(hOff, 0f);
		path.close();

		Matrix matrix = new Matrix();
		for(int i = 0; i < 4; ++i){
			matrix.setRotate(i * 90, centerX, centerY);
			path.transform(matrix);
			canvas.drawPath(path, mPaint);
		}
	}
}
