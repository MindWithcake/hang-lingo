package org.altervista.lemaialone.hanglingo.widgets.progress;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

/**
 * Draw a checkered grid over the image. Elements of the grid have a probability to be drawn
 * proportional to the percentage of image to be hidden. <br />
 * To chose if an element must be covered or not a random number is generated: if the number is
 * greater than the uncovered percentage then the element is drawn (to cover the underlying image).
 * The random number generator is initialized every time with the same seed, thus when the
 * percentage of uncovered image increases previously revealed elements will remain revealed. A
 * different seed is generated every time this class is instantiated, thus the revealed pattern
 * changes every time the word changes.
 *
 * Created by Ilario Sanseverino on 11/11/15.
 */
public class RandomCheckeredDrawer implements OverlayDrawer {
	private final static float STEPS = 10f;

	private final long mSeed = System.currentTimeMillis();
	private final Paint mPaint;

	public RandomCheckeredDrawer(){
		mPaint = new Paint();
		mPaint.setColor(Color.BLACK);
		mPaint.setStyle(Paint.Style.FILL);
	}

	@Override public void draw(Canvas canvas, int progress, float boundWidth, float boundHeight){
		Random rand = new Random(mSeed);
		float width = boundWidth / STEPS;
		float height = boundHeight / STEPS;
		for(int i = 0; i < boundWidth; i += width){
			for(int j = 0; j < boundHeight; j += height){
				int r = rand.nextInt(MAX_PROGRESS);
				if(r >= progress)
					canvas.drawRect(i, j, i + width, j + height, mPaint);
			}
		}
	}
}
