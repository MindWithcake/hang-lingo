package org.altervista.lemaialone.hanglingo.widgets;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * CardView with height always the same as width. To be used inside vertical grids.
 *
 * Created by Ilario Sanseverino on 15/10/15.
 */
public class SquareCardView extends CardView{
	public SquareCardView(Context context){
		super(context);
	}

	public SquareCardView(Context context, AttributeSet attrs){
		super(context, attrs);
	}

	public SquareCardView(Context context, AttributeSet attrs, int defStyleAttr){
		super(context, attrs, defStyleAttr);
	}

	@Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		//noinspection SuspiciousNameCombination
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}
}
