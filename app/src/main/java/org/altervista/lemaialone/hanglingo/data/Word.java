package org.altervista.lemaialone.hanglingo.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Data model for the records of type Word recieved from the server.
 *
 * Created by Ilario Sanseverino on 12/10/15.
 */
public class Word implements Parcelable {
	public final String word;
	public final String imageURI;

	public Word(String word, String imageURI){
		this.word = word;
		this.imageURI = imageURI;
	}

	protected Word(Parcel in){
		word = in.readString();
		imageURI = in.readString();
	}

	public static final Creator<Word> CREATOR = new Creator<Word>() {
		@Override public Word createFromParcel(Parcel in){
			return new Word(in);
		}

		@Override public Word[] newArray(int size){
			return new Word[size];
		}
	};

	@Override public int describeContents(){
		return 0;
	}

	@Override public void writeToParcel(Parcel dest, int flags){

		dest.writeString(word);
		dest.writeString(imageURI);
	}
}
