package org.altervista.lemaialone.hanglingo.widgets.progress;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;

import java.util.Random;

/**
 * Show the image in a radial way, with the origin in one of the corners. The image is parted into
 * 90 slices, 1 per degree, and each slice is drawn with a probability proportional to the covered
 * surface percentage. The random number generator is seeded every time with the same value, so
 * that when a bigger portion of the image is revealed the previously shown slices will still be
 * visible. The seed changes every time this class is instantiated, so that that reveal pattern
 * is not always the same.
 *
 * Created by Ilario Sanseverino on 12/11/15.
 */
public class CornerRadialDrawer implements OverlayDrawer {
	private final static float STEPS = 90f;

	private final long mSeed = System.currentTimeMillis();
	private final Paint mPaint;
	private final Matrix mMatrix;

	public CornerRadialDrawer(){
		mPaint = new Paint();
		mPaint.setColor(Color.BLACK);
		mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		mPaint.setAntiAlias(true);
		mMatrix = new Matrix();
	}

	@Override public void draw(Canvas canvas, int progress, float width, float height){
		if(progress == MAX_PROGRESS)
			return;
		if(progress == 0){
			canvas.drawPaint(mPaint);
			return;
		}

		Random rand = new Random(mSeed);
		Path path = new Path();
		path.moveTo(0f, 0f);

		float oldX = width, oldY = 0;
		for(int i = 1; i < STEPS; ++i){
			double rad = Math.toRadians(i);
			float x = Math.min((1f / (float)Math.tan(rad)) * width, width);
			float y = Math.min((float)Math.tan(rad) * height, height);
			if(rand.nextInt(MAX_PROGRESS) > progress){
				path.lineTo(oldX,oldY);
				path.lineTo(x, y);
				path.lineTo(0f, 0f);
			}
			oldX = x;
			oldY = y;
		}
		path.close();

		mMatrix.setRotate(rand.nextInt(4) * 90, width / 2, height / 2);
		path.transform(mMatrix);

		canvas.drawPath(path, mPaint);
	}
}
