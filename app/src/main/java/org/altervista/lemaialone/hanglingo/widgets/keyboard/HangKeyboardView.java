package org.altervista.lemaialone.hanglingo.widgets.keyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.data.DataProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Keyboard displaying only capital letters from the word's alphabet. Created by Ilario Sanseverino
 * on 28/07/15.
 */
public class HangKeyboardView extends KeyboardView {
	private List<Keyboard.Key> wrongs;
	private List<Keyboard.Key> rights;
	private KeyCheckListener mListener;
	private final Paint mTextPaint;

	public HangKeyboardView(Context context){
		this(context, null);
	}

	public HangKeyboardView(Context context, AttributeSet attrs){
		super(context, attrs);
		setOnKeyboardActionListener(new HangKeyboardAdapter());
		wrongs = new ArrayList<>();
		rights = new ArrayList<>();
		mTextPaint = new Paint();
		mTextPaint.setTextAlign(Paint.Align.CENTER);
		mTextPaint.setTextSize(48);
		mTextPaint.setColor(Color.WHITE);
		mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.HINTING_ON);

		if(isInEditMode())
			setKeyboard(DataProvider.getInstance(context).getKeyboard());
	}

	public void reset(){
		wrongs.clear();
		rights.clear();
		invalidateAllKeys();
	}

	@Override public void onDraw(Canvas canvas){
		if(getKeyboard() != null){
			List<Keyboard.Key> keys = getKeyboard().getKeys();
			for(Keyboard.Key key : keys){
				Drawable keyBackground;
				if(wrongs.contains(key))
					keyBackground = ContextCompat.getDrawable(getContext(), R.drawable.key_wrong);
				else if(rights.contains(key))
					keyBackground = ContextCompat.getDrawable(getContext(), R.drawable.key_right);
				else if(key.pressed)
					keyBackground = ContextCompat.getDrawable(getContext(), R.drawable.key_down);
				else
					keyBackground = ContextCompat.getDrawable(getContext(), R.drawable.key_up);

				int keyX = key.x + (key.width / 2);
				int keyY = key.y + (key.height / 2);
				int rad = Math.min(key.width, key.height) / 2;

				keyBackground.setBounds(keyX - rad, keyY - rad, keyX + rad, keyY + rad);
				keyBackground.draw(canvas);
				canvas.drawText(key.label.toString(), keyX, keyY + 18, mTextPaint);
			}
		}
	}

	public void setListener(KeyCheckListener listener){
		mListener = listener;
	}

	private class HangKeyboardAdapter extends KeyboardAdapter {
		@Override public void onKey(int primaryCode, int[] keyCodes){
			if(mListener == null)
				return;

			List<Keyboard.Key> keys = getKeyboard().getKeys();
			Keyboard.Key pressed = null;
			for(Keyboard.Key key : keys){
				if(key.codes[0] == primaryCode){
					pressed = key;
					break;
				}
			}
			if(pressed == null || wrongs.contains(pressed) || rights.contains(pressed))
				return;

			if(mListener.onKey(primaryCode))
				rights.add(pressed);
			else
				wrongs.add(pressed);
		}
	}
}
