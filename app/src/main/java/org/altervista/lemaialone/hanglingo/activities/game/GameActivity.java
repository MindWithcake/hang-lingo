package org.altervista.lemaialone.hanglingo.activities.game;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.altervista.lemaialone.hanglingo.R;
import org.altervista.lemaialone.hanglingo.data.Kandinskij;
import org.altervista.lemaialone.hanglingo.widgets.WordView;
import org.altervista.lemaialone.hanglingo.widgets.keyboard.HangKeyboardView;
import org.altervista.lemaialone.hanglingo.widgets.progress.OverlayDrawer;
import org.altervista.lemaialone.hanglingo.widgets.progress.ProgressRevealDrawable;

public class GameActivity extends GameFlowActivity {
	private String scoreLabel;
	private String countLabel;

	private WordView mWordView;
	private ProgressRevealDrawable mHintView;
	private TextView mWordCount, mScore;
	private HangKeyboardView mKeyboardView;
	private TextView mContinueButton;
	private View mButtonContainer;

	@Override protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		scoreLabel = getString(R.string.label_score);
		countLabel = getString(R.string.label_words);

		mWordView = (WordView)findViewById(R.id.hang_image);
		mHintView = (ProgressRevealDrawable)findViewById(R.id.score_image);
		mWordCount = (TextView)findViewById(R.id.words_count);
		mScore = (TextView)findViewById(R.id.score_count);
		mKeyboardView = (HangKeyboardView)findViewById(R.id.custom_keyboard);
		mContinueButton = (TextView)findViewById(R.id.continue_button);
		mButtonContainer = findViewById(R.id.button_container);
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);

		setSupportActionBar(toolbar);
		toolbar.setNavigationIcon(R.drawable.arrow_back);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v){ onBackPressed(); }
		});

		mKeyboardView.setKeyboard(provider.getKeyboard());
		mKeyboardView.setListener(this);
		mButtonContainer.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v){
				initMatch();
			}
		});

		Typeface scoreFace = Kandinskij.getFont(this, "fonts/FoglihtenPCS.otf");
		mScore.setTypeface(scoreFace);
		mWordCount.setTypeface(scoreFace);
		mHintView.setImageResource(R.mipmap.ic_launcher);
	}

	@Override protected void setScore(int current, int best){
		mWordCount.setText(String.format(countLabel, current));
		mScore.setText(String.format(scoreLabel, best));
	}

	@Override protected void startMatch(String word, String image){
		mWordView.setImageStep(0);
		mKeyboardView.reset();
		mKeyboardView.setVisibility(View.VISIBLE);
		mButtonContainer.setVisibility(View.GONE);

		mWordView.setWord(word);
		if(!image.isEmpty()){
			mHintView.setOverlayDrawer(provider.nextOverlay());
			mHintView.setProgress(0);
			Kandinskij.load(this, image).into(mHintView);
		}
		else{
			mHintView.setOverlayDrawer(null);
			Kandinskij.load(this, R.mipmap.ic_launcher).into(mHintView);
		}
	}

	@Override protected void noMatch(){
		mWordView.setWord(getString(R.string.label_no_words));
		mKeyboardView.setVisibility(View.INVISIBLE);
		mButtonContainer.setVisibility(View.VISIBLE);
		mContinueButton.setText(getString(R.string.label_change));
		mButtonContainer.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v){ finish(); }
		});
	}

	@Override protected void endGame(boolean isWon, String fullWord){
		mWordView.setWord(fullWord);
		mHintView.setProgress(OverlayDrawer.MAX_PROGRESS);
		mButtonContainer.setVisibility(View.VISIBLE);
		mKeyboardView.setVisibility(View.INVISIBLE);
	}

	@Override protected void showLoadingProgress(String progress){
		mWordView.setWord(progress);
	}

	@Override protected void resumeGame(String word, int tries){
		mWordView.setWord(word);
		mWordView.setImageStep(tries);
		mKeyboardView.setVisibility(View.VISIBLE);
	}

	@Override protected void advance(String word, float progress){
		mWordView.setWord(word);
		mHintView.setProgress((int)(progress * OverlayDrawer.MAX_PROGRESS));
	}

	@Override protected void fail(int tries){
		mWordView.setImageStep(tries);
	}
}
